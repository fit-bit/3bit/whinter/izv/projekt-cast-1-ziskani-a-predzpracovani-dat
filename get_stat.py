"""
Funkce v tomto souboru umožnuje zpracovat data načtená DataDownloader​em a zobrazit je v pžehledné podobě do obrázku
"""
import download

import argparse
import os
import matplotlib.pyplot as plt


def plot_stat(data_source, fig_location = None, show_figure = False):
    """
    Funkce vezme data zpracovaná z DataDownloader​, načte
    je a vizualizuje počty nehod.
    - Pro každý rok bude jeden samostatný podgraf.
    - Jednotlivé grafy budou sloupcové, kdy na ose x bude právě zvolený kraj.
    - Sloupce budou anotované pořadím počtu nehod v daném kraji (t.j. kraj s největším
      počtem nehod bude mít nad patřičným sloupcem 1., atd).
    - Pokud bude nastavený “​ fig_location​ ”, tak se do dané adresy uloží obrázek.
      Pokud by složka, kam se má obrázek ukládat, neexistovala, vytvoří se.
    - Pokud je nastavený parametr “​ show_figure​ ”, tak se graf zobrazí v okně
      (​ plt.show()​ ).
    """

    # Rozřazení dat podle let pro grafy
    data_sorted = {}

    # Získání záznamů podle let a krajů
    for line in range(data_source[1][0].shape[0]):
      try:
        data_sorted[str(data_source[1][3][line])[:4]]
      except:
        data_sorted[str(data_source[1][3][line])[:4]] = {}

      try:
        data_sorted[str(data_source[1][3][line])[:4]][str(data_source[1][-1][line])]
      except:
        data_sorted[str(data_source[1][3][line])[:4]][str(data_source[1][-1][line])] = []

      data_sorted[str(data_source[1][3][line])[:4]][str(data_source[1][-1][line])].append([item[line] for item in data_source[1]])

    # Vytvoření obrázku s potřebným počtem podgrafů
    figure, image = plt.subplots(len(data_sorted.keys()), sharex = True, figsize=(10,15))

    # Seřazení let pro výpis a tvorba podgrafů pro jednotlivé roky
    sorted_years = sorted(data_sorted.keys())
    for year_index in range(len(data_sorted.keys())):
      x = data_sorted[sorted_years[year_index]].keys()
      y = []
      for region in x:
        y.append(len(data_sorted[sorted_years[year_index]][region]))

      # Nastavení pomocné čáry u y osy
      image[year_index].grid(axis = 'y')
      image[year_index].set_axisbelow(True)

      image[year_index].set_title(sorted_years[year_index])
      image[year_index].set_ylabel('Počet nehod')
      rects = image[year_index].bar(x, y, color = 'red', alpha = 0.7)

      # Přidání pořadového čísla nad prvky grafu
      # seřazení čísel podle velikosti, aby šli správně přiřadit pořadová čísla
      sorted_values = sorted(y, reverse = True)
      for rect in rects:
        height = rect.get_height()
        image[year_index].text(rect.get_x() + rect.get_width()*0.5, 1.01*height, str(sorted_values.index(height)+1)+'.', ha='center', va='bottom')

      # Upravení měřítka os aby se vlezly popisky sloupců
      ymin, ymax = image[year_index].get_ylim()
      image[year_index].set_ylim(ymin * 1.2, ymax * 1.2)

    figure.suptitle("Počet nehod v jednotlivých krajích ČR", fontsize="x-large")
    plt.subplots_adjust(left=0.125,
                    bottom=0.1, 
                    right=0.9, 
                    top=0.9, 
                    wspace=0.2, 
                    hspace=0.35)

    # Uložení grafu
    # pokud neexistuje cesta pro uložení grafu, vytvoří se potřebné složky
    if fig_location != None:
      if not os.path.exists(os.path.dirname(fig_location)):
            os.makedirs(os.path.dirname(fig_location))

      figure.savefig(fig_location)

    if show_figure == True:
      plt.show()


if __name__ == "__main__":
    """
    Pokud bude skript spuštěný jako hlavní (t.j. nebude importovaný jako modul) pomocí příkazové řádky,
    je možné zadat parametry --fig_location a --show_figure.
    """

    # Zpracování argumentů
    parser = argparse.ArgumentParser()
    parser.add_argument('--fig_location', help='Adresa pro uložení obrázku')
    parser.add_argument('--show_figure', help='Zobrazení grafu')
    args = parser.parse_args()

    data_source = download.DataDownloader().get_list()

    # nastavení argumentů
    show_figure = ""
    if args.show_figure == "True":
      show_figure = True
    else:
      show_figure = False

    plot_stat(data_source, args.fig_location, show_figure)

""" Konec get_stat.py """