"""
Soubor obsahuje třídu která umožňuje načtení dat nehodovosti ve krajích ČR 
"""

import os
import requests
import re
import zipfile
import csv
import gzip
import pickle
import numpy as np

class DataDownloader:
    def __init__(self, url = "https://ehw.fit.vutbr.cz/izv/", folder = "data", cache_filename = "data_{}.pkl.gz"):
        """
        inicializátor - obsahuje volitelné parametry:
            url​ - ukazuje, z jaké adresy se data načítají. Defaultně bude nastavený na
                  výše uvedenou URL.
            folder​ - říká, kam se mají dočasná data ukládat. Tato složka nemusí na
                     začátku existovat!
            cache_filename​ - jméno souboru ve specifikované složce, které říká, kam
                             se soubor s již zpracovanými daty z funkce ​ get_list​ bude ukládat a odkud
                             se budou data brát pro další zpracování a nebylo nutné neustále stahovat
                             data přímo z webu. Složené závorky (formátovací řetězec) bude nahrazený
                             tříznakovým kódem příslušného kraje. Podporovaný je pouze formát “pickle” s kompresí gzip.
        """

        self.region_codes = {"PHA": "00", "STC": "01", "JHC": "02", "PLK": "03", "KVK": "19", "ULK": "04", "LBK": "18", "HKK": "05", "PAK": "17", "OLK": "14", "MSK": "07", "JHM": "06", "ZLK": "15", "VYS": "16"}

        self.url = url

        self.folder = folder
        self.cache_filename = cache_filename
        self.loaded = {}

        # Tvorba složky s pomocnými když neexistuje
        if not os.path.exists(folder):
            os.mkdir(folder)

    def download_data(self):
        """ funkce stáhne do datové složky folder​ všechny soubory s daty z adresy ​ url​ """
        
        page_content = requests.get(url = self.url, headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36"}).text

        # Najití odkazů na souboru které chceme stáhnout
        data_urls = re.findall("data/[^ ]*.zip", page_content)

        # Stáhnutí zip souborů z internetu
        for file_url in data_urls:
            # získání jména souboru
            file_name = file_url.replace('data/', '')

            # Kontrola jestli už není soubor ve složce stažený
            if not os.path.exists(self.folder+"/"+file_name):
                # Uložení souboru
                zip_file = requests.get(self.url+file_url, stream = True)
                with open(self.folder+"/"+file_name, 'wb') as fd:
                    for chunk in zip_file.iter_content(chunk_size=128):
                        fd.write(chunk)

    def parse_region_data(self, region):
        """
        Pokud nejsou data pro daný kraj stažená, stáhnou se do datové složky folder​ . Poté
        je pro daný region specifikovaný tříznakovým kódem vždy
        vyparsuje do následujícího formátu dvojice (​ tuple​ ), kde první položka je seznam
        (​ list​ ) řetězců a druhá položka bude seznam (​ list​ ) NumPy polí, schematicky:
                            tuple(list[str], list[np.ndarray])
        Seznam řetězců odpovídá názvům jednotlivých datových sloupců, NumPy pole
        budou obsahovat data. Platí, že délka obou seznamů je stejná, ​ shape ​ všech
        NumPy polí je stejný. Při parsování je přidaný nový sloupec “region”, který
        obsahuje tříznaký kód patřičného kraje, tj. odpovídá hodnotě ​ region​ .
        """
        # Lepší projít všechny data, co kdyby některý záznam omylem nepřekopírovaly  ¯\_(ツ)_/¯

        # stáhnutí všech dat
        self.download_data()

        # hlavička dat
        #header = ["Identifikační číslo", "Druh pozemní komunikace", "Číslo pozemní komunikace", "Den, měsíc, rok", "Den v týdnu", "Čas", "Druh nehody", "Druh srážky jedoucích vozidel", "Druh pevné překážky", "Charakter nehody", "Zavinění nehody", "Alkohol u viníka nehody přítomen", "Hlavní příčiny nehody", "Usmrceno osob", "Těžce zraněno osob", "Lehce zraněno osob", "Celková hmotná škoda", "Druh povrchu vozovky", "Stav povrchu vozovky v době nehody", "Stav komunikace", "Povětrnostní podmínky v době nehody", "Viditelnost", "Rozhledové poměry", "Dělení komunikace", "Situování nehody na komunikaci", "Řízení provozu v době nehody", "Místní úprava přednosti v jízdě", "Specifická místa a objekty v místě nehody", "Směrové pohledy", "Počet zúčastněných vozidel", "Místo dopravní nehody", "Druh křižující komunikace", "Druh vozidla", "Výrobní značka motorového vozidla", "Rok výroby vozidla", "Charakteristika vozidla", "Smyk", "Vozidlo po nehodě", "Únik pohoných, přepravovanách hmot", "Způsob vyproštění osob z vozidla", "Směr jízdy nebo postavení vozidla", "Škoda na vozidle", "Kategorie řidiče", "Stav řidiče", "Vnější ovlivnění řidiče", "a", "b", "GPS: souřadnice X",	"GPS: souřadnice Y", "f", "g", "h", "i", "j", "k", "l", "n", "o", "p", "q", "r", "s", "t", "Lokalita nehody", "Region"]
        header = ["p1", "p36", "p37", "p2a", "weekday(p2a)", "p2b", "p6", "p7",	"p8", "p9",	"p10", "p11", "p12", "p13a", "p13b", "p13c", "p14", "p15", "p16", "p17", "p18", "p19", "p20", "p21", "p22", "p23", "p24", "p27", "p28", "p34", "p35", "p39", "p44", "p45a", "p47", "p48a", "p49", "p50a", "p50b", "p51", "p52", "p53", "p55a", "p57", "p58", "a", "b", "d", "e", "f", "g", "h", "i", "j", "k", "l", "n", "o", "p", "q", "r", "s", "t", "p5a", "region"]
        items_tmp = {}
        arrays = []

        for item in header:
            arrays.append([])

        # Postupné projití zip souborů souborů ve složce a vybrání údajů o krajích
        files = os.listdir(self.folder)
        for file_name in files:
            if file_name.endswith(".zip"):
                zf = zipfile.ZipFile(self.folder+"/"+file_name, 'r')
                
                # vyčtení příslušního CSV ze .zip souboru
                csv_file = zf.read(self.region_codes[region]+".csv")
                csv_string = csv_file.decode("ISO-8859-1").splitlines()

                csv_reader = csv.reader(csv_string, delimiter=';')

                # Přidání záznamu do slovníku
                # Duplicitní záznami se neukládají
                for row in csv_reader:
                    if row[0] not in items_tmp.keys():
                        items_tmp[row[0]] = row
        
        # Převedení prvků na vhodný numpy formát
        for row in items_tmp.values():
            for item in range(len(row)):
                # přetypování jednotlivých položek
                if header[item] == "p1":
                    # Identifikační číslo 
                    arrays[item].append(int(row[item]))
              
                elif header[item] == "p36":
                    # Druh pozemní komunikace
                    arrays[item].append(int(row[item]))
             
                elif header[item] == "p37":
                    #  Číslo pozemní komunikace
                    try: 
                        arrays[item].append(int(row[item]))
                    except:
                        arrays[item].append(np.nan)
 
                elif header[item] == "p2a":
                    # Den, měsíc, rok
                    arrays[item].append(np.datetime64(row[item]))
 
                elif header[item] == "weekday(p2a)":
                    # Den v týdnu
                    arrays[item].append(int(row[item]))

                elif header[item] == "p2b":
                    # Čas
                    minutes = row[item][-2:]
                    hours = row[item][:-2]

                    if ((hours == "25") or (minutes == "60")):
                        arrays[item].append(np.datetime64('nat'))
                    else:
                        arrays[item].append(np.datetime64(hours+minutes))

                elif header[item] == "p6":
                    # Druh nehody
                    arrays[item].append(int(row[item])) 
                    
                elif header[item] == "p7":
                    # Druh srážky jedoucích vozidel
                    arrays[item].append(int(row[item]))
                    
                elif header[item] == "p8":
                    # Druh pevné překážky
                    arrays[item].append(int(row[item]))
                    
                elif header[item] == "p9":
                    # Charakter nehody
                    arrays[item].append(int(row[item]))
                    
                elif header[item] == "p10":
                    # Zavinění nehody
                    arrays[item].append(int(row[item]))
                    
                elif header[item] == "p11":
                    # Alkohol u viníka nehody přítomen
                    arrays[item].append(int(row[item]))

                elif header[item] == "p12":
                    # Hlavní příčiny nehody
                    arrays[item].append(int(row[item]))

                elif header[item] == "p13a":
                    # Usmrceno osob
                    arrays[item].append(int(row[item]))
                    
                elif header[item] == "p13b":
                    # Těžce zraněno osob    
                    arrays[item].append(int(row[item]))

                elif header[item] == "p13c":
                    # Lehce zraněno osob
                    arrays[item].append(int(row[item]))

                elif header[item] == "p14":
                    # Celková hmotná škoda
                    arrays[item].append(int(row[item]))

                elif header[item] == "p15":
                    # Druh povrchu vozovky
                    arrays[item].append(int(row[item]))

                elif header[item] == "p16":
                    # Stav povrchu vozovky v době nehody
                    arrays[item].append(int(row[item]))

                elif header[item] == "p17":
                    # Stav komunikace
                    arrays[item].append(int(row[item]))

                elif header[item] == "p18":
                    # Povětrnostní podmínky v době nehody
                    arrays[item].append(int(row[item]))

                elif header[item] == "p19":
                    # Viditelnost
                    arrays[item].append(int(row[item]))

                elif header[item] == "p20":
                    # Rozhledové poměry
                    arrays[item].append(int(row[item]))

                elif header[item] == "p21":
                    # Dělení komunikace
                    arrays[item].append(int(row[item]))

                elif header[item] == "p22":
                    # Situování nehody na komunikaci
                    arrays[item].append(int(row[item]))

                elif header[item] == "p23":
                    # Řízení provozu v době nehody
                    arrays[item].append(int(row[item]))

                elif header[item] == "p24":
                    # Místní úprava přednosti v jízdě
                    arrays[item].append(int(row[item]))

                elif header[item] == "p27":
                    # Specifická místa a objekty v místě nehody
                    arrays[item].append(int(row[item]))

                elif header[item] == "p28":
                    # Směrové pohledy
                    arrays[item].append(int(row[item]))

                elif header[item] == "p34":
                    # Počet zúčastněných vozidel
                    arrays[item].append(int(row[item]))

                elif header[item] == "p35":
                    # Místo dopravní nehody 
                    arrays[item].append(int(row[item]))

                elif header[item] == "p39":
                    # Druh křižující komunikace
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p44":
                    # Druh vozidla
                    arrays[item].append(int(row[item]))
                    
                elif header[item] == "p45a":
                    # Výrobní značka motorového vozidla
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p47":
                    # Rok výroby vozidla
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p48a":
                    # Charakteristika vozidla
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p49":
                    # Smyk
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p50a":
                    # Vozidlo po nehodě
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p50b":
                    # Únik pohoných, přepravovanách hmot
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)

                elif header[item] == "p51":
                    # Způsob vyproštění osob z vozidla
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)

                elif header[item] == "p52":
                    # Směr jízdy nebo postavení vozidla
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                        
                elif header[item] == "p53":
                    # Škoda na vozidle
                    arrays[item].append(int(row[item]))
                   
                elif header[item] == "p55a":
                    # Kategorie řidiče
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "p57":
                    # Stav řidiče
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)

                elif header[item] == "p58":
                    # Vnější ovlivnění řidiče
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)

                elif header[item] == "a":
                    # Nahrazení desetinné tečky za čárku
                    good_float = str(row[item]).replace(",",".")
                    try: 
                        arrays[item].append(float(good_float))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "b":
                    # Nahrazení desetinné tečky za čárku
                    good_float = str(row[item]).replace(",",".")
                    try: 
                        arrays[item].append(float(good_float))
                    except:
                        arrays[item].append(np.nan)

                elif header[item] == "d":
                    # GPS: souřadnice X
                    # Nahrazení desetinné tečky za čárku
                    good_float = str(row[item]).replace(",",".")
                    try: 
                        arrays[item].append(float(good_float))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "e":
                    # GPS: souřadnice Y
                    # Nahrazení desetinné tečky za čárku
                    good_float = str(row[item]).replace(",",".")
                    try: 
                        arrays[item].append(float(good_float))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "f":
                    # Nahrazení desetinné tečky za čárku
                    good_float = str(row[item]).replace(",",".")
                    try: 
                        arrays[item].append(float(good_float))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "g":
                    # Nahrazení desetinné tečky za čárku
                    good_float = str(row[item]).replace(",",".")
                    try: 
                        arrays[item].append(float(good_float))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "h":
                    arrays[item].append(row[item])
                    
                elif header[item] == "i":
                    arrays[item].append(row[item])
                    
                elif header[item] == "j":
                    arrays[item].append(row[item])
                    
                elif header[item] == "k":
                    arrays[item].append(row[item])
                    
                elif header[item] == "l":
                    arrays[item].append(row[item])
                    
                elif header[item] == "n":
                    arrays[item].append(row[item])
                    
                elif header[item] == "o":
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)

                elif header[item] == "p":
                    arrays[item].append(row[item])
                    
                elif header[item] == "q":
                    arrays[item].append(row[item])
                    
                elif header[item] == "r":
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "s":
                    try: 
                        arrays[item].append(float(row[item]))
                    except:
                        arrays[item].append(np.nan)
                    
                elif header[item] == "t":
                    arrays[item].append(row[item])
                    
                elif header[item] == "p5a":
                    # Lokalita nehody
                    arrays[item].append(int(row[item]))
                    
                else:
                    pass
                
            # přidání označení kraje
            arrays[-1].append(region)

        
        # Vytvoření návratových polí ve správném formátu
        for item in range(len(arrays)):
            arrays[item] = np.array(arrays[item])

        return (header, arrays)

    def get_list(self, regions = None):
        """
        Vrací zpracovaná data pro vybrané kraje (regiony). Argument ​ regions ​ specifikuje
        seznam (list) požadovaných krajů jejich třípísmennými kódy. Pokud seznam není
        uveden (je použito None), zpracují se všechny kraje včetně Prahy. Výstupem funkce
        je dvojice ve stejném formátu, jako návratová hodnota funkce
        parse_region_data​ .

        Pro každý kraj získá data s využitím funkce ​ parse_region_data ​ tak, že se
        budou výsledky uchovávat v paměti (v atributu instance třídy "loaded") a ukládat do
        pomocného cache souboru pomocí následujícího schématu:
        - pokud už je výsledek načtený v paměti (tj. dostupný ve vámi zvoleném
          atributu), vrátí tuto dočasnou kopii
        - pokud není uložený v paměti, ale je již zpracovaný v cache souboru, tak
          načte výsledek z cache, uloží jej do atributu a vrátí.
        - jinak se zavolá funkce ​ parse_region_data, výsledek volání se
          uloží do cache, poté do paměti a výsledek vrátí
        """

        # hlavička dat
        #header = ["Identifikační číslo", "Druh pozemní komunikace", "Číslo pozemní komunikace", "Den, měsíc, rok", "Den v týdnu", "Čas", "Druh nehody", "Druh srážky jedoucích vozidel", "Druh pevné překážky", "Charakter nehody", "Zavinění nehody", "Alkohol u viníka nehody přítomen", "Hlavní příčiny nehody", "Usmrceno osob", "Těžce zraněno osob", "Lehce zraněno osob", "Celková hmotná škoda", "Druh povrchu vozovky", "Stav povrchu vozovky v době nehody", "Stav komunikace", "Povětrnostní podmínky v době nehody", "Viditelnost", "Rozhledové poměry", "Dělení komunikace", "Řízení provozu v době nehody", "Místní úprava přednosti v jízdě", "Specifická místa a objekty v místě nehody", "Směrové pohledy", "Počet zúčastněných vozidel", "Místo dopravní nehody", "Druh křižující komunikace", "Druh vozidla", "Výrobní značka motorového vozidla", "Rok výroby vozidla", "Charakteristika vozidla", "Smyk", "Vozidlo po nehodě", "Únik pohoných, přepravovanách hmot", "Způsob vyproštění osob z vozidla", "Směr jízdy nebo postavení vozidla", "Škoda na vozidle", "Kategorie řidiče", "Stav řidiče", "Vnější ovlivnění řidiče", "a", "b", "GPS: souřadnice X",	"GPS: souřadnice Y", "f", "g", "h", "i", "j", "k", "l", "n", "o", "p", "q", "r", "s", "t", "Lokalita nehody", "Region"]
        header = ["p1", "p36", "p37", "p2a", "weekday(p2a)", "p2b", "p6", "p7",	"p8", "p9",	"p10", "p11", "p12", "p13a", "p13b", "p13c", "p14", "p15", "p16", "p17", "p18", "p19", "p20", "p21", "p22", "p23", "p24", "p27", "p28", "p34", "p35", "p39", "p44", "p45a", "p47", "p48a", "p49", "p50a", "p50b", "p51", "p52", "p53", "p55a", "p57", "p58", "a", "b", "d", "e", "f", "g", "h", "i", "j", "k", "l", "n", "o", "p", "q", "r", "s", "t", "p5a", "region"]
        arrays = []

        for item in header:
            arrays.append(np.array([]))

        # Zpracování zadaných krajů, pokud není nic zadáno zpracují se všechny
        if regions == None:
            regions = self.region_codes.keys()

        for region in regions:
            # Pokud je zadaný neznámí kraj, tak se přezkočí
            if region not in self.region_codes:
                continue

            # Kontrola jestli už data nejsou načtena a nebo v casche
            # TODO co když na serveru udělají změnu souborů?
            if region in self.loaded.keys():
                # Data jsou v paměti, jen se tedy vrátí
                result = self.loaded[region]

            elif os.path.exists(self.folder+"/"+self.cache_filename.format(region)):
                # Existuje cash soubor, načte se do paměti
                result = pickle.load( gzip.open(self.folder+"/"+self.cache_filename.format(region), "rb" ))
                self.loaded[region] = result

            else:
                # stažení dat z internetu
                result = self.parse_region_data(region)

                # ulože do paměti a casche
                pickle.dump(result, gzip.open(self.folder+"/"+self.cache_filename.format(region), "wb" ) )

                self.loaded[region] = result

            # napojení dat za sebe
            for item in range(len(result[1])):
                # Natavení prvního pole na správný formát
                if (arrays[item].shape[0] == 0):
                    arrays[item] = np.array([], dtype=result[1][item].dtype)
                
                arrays[item] = np.append(arrays[item], result[1][item])

        return (header, arrays)

if __name__ == "__main__":
    """
    Pokud bude skript spuštěný jako hlavní (t.j. nebude importovaný jako modul)​​ , stáhne data
    pro 3 vybrané kraje (s využitím funkce ​ get_list​ ) a vypište do konzole základní
    informace o stažených datech (jaké jsou sloupce, počet záznamů a jaké kraje jsou v
    datasetu).
    """ 

    regions = ["JHM", "MSK", "PHA"]
    result = DataDownloader().get_list(regions)

    # Výtisk hodnot
    print("Kraje v záznamech: ", np.unique(result[1][-1]))
    print("Datové sloupce: ", result[0])
    print("Počet záznamů: ", result[1][0].shape[0])

""" Konec download.py """